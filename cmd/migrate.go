package cmd

import (
	"path/filepath"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/config"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/db"
)

var doDryRun bool
var useTestDB bool

func init() {
	rootCmd.AddCommand(migrateCmd)
	migrateCmd.Flags().BoolVarP(&doDryRun, "dryrun", "d", false, "simulate migrations but do not change database")
	migrateCmd.Flags().BoolVarP(&useTestDB, "testdb", "t", false, "run migrations against the test database")
}

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Run migrations against the Postgres database",
	Long:  `Run migrations against the Postgres database idempotently`,
	Run:   migrate,
}

func migrate(cmd *cobra.Command, args []string) {
	log.WithFields(log.Fields{
		"use_test":          useTestDB,
		"postgres_url":      config.DatabaseURL,
		"postgres_test_url": config.DatabaseTestURL,
	}).Debug("running migrations")
	var d *sqlx.DB
	if useTestDB {
		d = sqlx.MustConnect("postgres", config.DatabaseTestURL)
		log.Debug("migrating test db")
	} else {
		d = sqlx.MustConnect("postgres", config.DatabaseURL)
		log.Debug("migrating live db")
	}
	version := db.GetCurrentVersion(d)
	log.WithFields(log.Fields{
		"version": version,
	}).Info("current database migration version")
	dir := filepath.Join(".", "./server/migrations")
	db.Migrate(d, dir, doDryRun)
	return
}
