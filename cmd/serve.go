package cmd

import (
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/middleware"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/config"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/controllers/auth"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/controllers/clientlog"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/controllers/status"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/controllers/todo"
)

func init() {
	rootCmd.AddCommand(serveCmd)
}

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the API server",
	Run: func(cmd *cobra.Command, args []string) {
		s := server.NewServer()
		s.DB = sqlx.MustConnect("postgres", config.DatabaseURL)

		// make a JWT protected group
		api := s.Group("/api")
		jwtConfig := middleware.JWTConfig{
			Claims:     &auth.CustomClaims{},
			SigningKey: []byte(config.JWTSalt),
		}
		api.Use(middleware.JWTWithConfig(jwtConfig))

		auth.Install(s)
		auth.InstallOnGroup(s, api)
		status.Install(s)
		clientlog.Install(s)
		// install Todo routes on this protected group
		todo.InstallOnGroup(s, api)

		s.File("/", "client/static/index.html")
		s.Static("/static", "client/static")
		s.Static("/assets", "client/static/assets")

		log.Fatal(s.Run())
	},
}
