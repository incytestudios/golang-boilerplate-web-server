package models

import "time"

// AutoFields represents collumns that are common to all our tables
// it is meant to be included by other model structs
type AutoFields struct {
	ID           int       `db:"id" json:"id"`
	DateCreated  time.Time `db:"date_created" json:"date_created"`
	DateModified time.Time `db:"date_modified" json:"date_modified"`
}
