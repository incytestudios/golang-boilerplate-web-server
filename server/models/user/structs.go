package user

import "gitlab.com/incytestudios/golang-boilerplate-web-server/server/models"

// User represents a "User" model
type User struct {
	models.AutoFields
	Email string `db:"email" json:"email"`
	Name  string `db:"name" json:"name"`
}
