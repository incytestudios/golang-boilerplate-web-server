package user

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/config"
)

// Authenticate takes email and password and returns user
func Authenticate(d *sqlx.DB, email string, password string) (*User, error) {
	query := fmt.Sprintf(`
    SELECT
      id,
      email,
      name,
      date_created,
      date_modified
    FROM
      users
    WHERE
      email=lower($1)
		AND
		password = crypt($2, '%s')
  `, config.DBSalt)
	var out User
	err := d.Get(&out, query, email, password)
	return &out, err
}

// CreateUser creates a new user item and returns the id
func CreateUser(d *sqlx.DB, name, email, password string) (*User, error) {
	u := &User{
		Name:  name,
		Email: email,
	}
	query := fmt.Sprintf(`
	INSERT INTO 
	  users (name, email, password) 
	VALUES 
	  ($1, $2, crypt($3, '%s')) 
	RETURNING 
	  id, date_created, date_modified
    `, config.DBSalt)
	row := d.QueryRow(query, name, email, password)
	err := row.Scan(&u.ID, &u.DateCreated, &u.DateModified)
	return u, err
}

// SetPassword replaces the password for a user
func SetPassword(d *sqlx.DB, id int, password string) error {
	log.WithField("user id", id).Debug("updating password")
	query := fmt.Sprintf(`
  UPDATE
    users
  SET
    password = crypt($2, '%s')
  WHERE
    id = $1
  `, config.DBSalt)
	res, err := d.Exec(query, id, password)
	if err != nil {
		return err
	}
	rows, err := res.RowsAffected()
	if rows != 1 {
		return sql.ErrNoRows
	}
	return err
}
