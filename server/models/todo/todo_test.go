package todo

import (
	"testing"

	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/db"
)

func TestCreateReturnsID(t *testing.T) {
	d := db.MustGetTestDB()
	defer db.TruncateTable(d, "todos")
	t1, err := NewTodo(d, "test1")
	db.ErrCheck(err, "couldn't make todo")
	t2, err := NewTodo(d, "test2")
	db.ErrCheck(err, "couldn't make todo")
	if t1.ID >= t2.ID {
		t.Error("first ID should be lower than second")
	}
	check, err := GetByID(d, t1.ID)
	if err != nil {
		t.Error("known ID was not found " + err.Error())
	}
	if check.ID != t1.ID {
		t.Error("get by ID did not return expected item")
	}
}
