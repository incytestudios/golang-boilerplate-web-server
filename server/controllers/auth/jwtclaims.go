package auth

import (
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// CustomClaims extends the default claims with anything we want
// added to a user's JWT
type CustomClaims struct {
	ID           int       `json:"id"`
	Name         string    `json:"name"`
	Email        string    `json:"email"`
	DateCreated  time.Time `json:"date_created"`
	DateModified time.Time `json:"date_modified"`
	// TODO: IsAdmin bool   `json:"admin"`
	jwt.StandardClaims
}
