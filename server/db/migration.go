package db

import (
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strconv"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
)

func init() {
	log.SetFormatter(&log.TextFormatter{
		TimestampFormat: "2018-01-31T15:01:01.000",
		FullTimestamp:   true,
		ForceColors:     true,
	})
}

var (
	// our migration files must start with 3 integers followed by an underscore
	reMigrationPrefix = regexp.MustCompile(`^[0-9]{3}_.+`)
)

// Just a helper struct to represent each migration file we find
type migrationFile struct {
	AbsolutePath string
	Filename     string
	Version      int
}

func (mf *migrationFile) AsFields() log.Fields {
	return log.Fields{
		"AbsolutePath": mf.AbsolutePath,
		"Version":      mf.Version,
	}
}

// Scan our migrations folder for files that look like migrations and return
// migrationFile structs that represent them in proper order
func findPotentialMigrations(migrationDir string) []migrationFile {
	out := []migrationFile{}
	files, err := ioutil.ReadDir(migrationDir)
	ErrCheckWithFields(err, "Listing migration directory", log.Fields{
		"directory": migrationDir,
	})
	// we'll use this to determine if we skipped a number or double defined one
	dedupeCounter := 0
	for _, f := range files { // these should be sorted by the ReadDir call above
		// find entries that look like our migrations and are just regular files
		if f.Mode().IsRegular() && reMigrationPrefix.MatchString(f.Name()) {
			version, err := strconv.Atoi(f.Name()[0:3])
			switch {
			case version == dedupeCounter:
				log.WithFields(log.Fields{
					"version": version,
				}).Fatal("duplicate migration versions detected! ", version)
			case version != dedupeCounter+1:
				log.WithFields(log.Fields{
					"version": dedupeCounter,
				}).Fatal("gap in migration numbers detected! ", dedupeCounter)
			default:
				dedupeCounter = version
			}
			ErrCheckWithFields(err, "converting file to integer version", log.Fields{
				"directory": migrationDir,
				"file":      f.Name(),
			})
			mf := migrationFile{
				AbsolutePath: filepath.Join(migrationDir, f.Name()),
				Filename:     f.Name(),
				Version:      version}
			out = append(out, mf)
		}
	}
	return out
}

// GetCurrentVersion returns the current schema version as an integer, or nil with an error
func GetCurrentVersion(d *sqlx.DB) int {
	currentVersion := 0
	d.QueryRow(`
		SELECT max(current_version) FROM meta.migrations
	`).Scan(&currentVersion)
	return currentVersion
}

func isMigrationsTablePresent(d *sqlx.DB) (exists bool) {
	err := d.QueryRow(`
		SELECT EXISTS (
			SELECT *
			FROM information_schema.tables
			WHERE
				table_schema = 'meta'
				AND
				table_name = 'migrations'
		)
	`).Scan(&exists)
	ErrCheck(err, "looking for migrations table")
	return
}

func bootstrap(d *sqlx.DB, migrationDir string) {
	if isMigrationsTablePresent(d) {
		return
	}
	log.Info("creating migrations table")
	// find the bootstrap.sql file and run it
	bs, err := ioutil.ReadFile(filepath.Join(migrationDir, "bootstrap.sql"))
	ErrCheck(err, "reading contents of bootstrap.sql")
	sql := string(bs)
	_, err = d.Exec(sql)
	ErrCheck(err, "bootstrapping schema")
}

// Migrate ensures the db supports migration, loads bootstrap info, and applies
// as many migrations as it can in order until there are no migrations left to
// run, or a migration produces an error.
func Migrate(d *sqlx.DB, migrationDir string, dryrun bool) {
	log.WithFields(log.Fields{
		"dryrun": dryrun,
	}).Info("starting migration")

	exists := isMigrationsTablePresent(d)
	currentVersion := 0 // assume 0 unless we find out otherwise

	if exists {
		log.Info("migrations table found")
		currentVersion = GetCurrentVersion(d)
	} else {
		if dryrun {
			log.Info("would bootstrap database")
		} else {
			bootstrap(d, migrationDir)
		}
	}

	migrations := findPotentialMigrations(migrationDir)
	count := 0
	for _, mf := range migrations {
		if mf.Version > currentVersion {
			if dryrun {
				log.WithFields(mf.AsFields()).Info("would apply migration")
			} else {
				mustApplyMigration(d, mf)
			}
			count++
		}
	}
	if dryrun {
		log.WithFields(log.Fields{
			"migrations_to_apply": count,
		}).Info("would apply migrations")
	} else {
		log.WithFields(log.Fields{
			"migrations_applied": count,
		}).Info("migrations finished")
	}
}

// Apply a numbered migration to the database in a transaction
func mustApplyMigration(d *sqlx.DB, mf migrationFile) {
	log.Println("applying migration:", mf.Filename)

	bs, err := ioutil.ReadFile(mf.AbsolutePath)
	ErrCheckWithFields(err, "reading migration file from disk", mf.AsFields())
	sql := string(bs)

	tx, err := d.Begin()
	_, err = tx.Exec(sql) // the actual migration
	if err != nil {
		tx.Rollback()
		ErrCheckWithFields(err, "migration failed", mf.AsFields())
	}
	_, err = tx.Exec(`
		INSERT INTO
			meta.migrations (
				current_version,filename
			) VALUES (
				$1, $2
			);
		`, mf.Version, mf.Filename)
	if err != nil {
		tx.Rollback()
		ErrCheckWithFields(err, "updating migrations table failed", mf.AsFields())
	}
	tx.Commit()
}
